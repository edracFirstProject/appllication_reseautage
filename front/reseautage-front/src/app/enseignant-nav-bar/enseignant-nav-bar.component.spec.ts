import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantNavBarComponent } from './enseignant-nav-bar.component';

describe('EnseignantNavBarComponent', () => {
  let component: EnseignantNavBarComponent;
  let fixture: ComponentFixture<EnseignantNavBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnseignantNavBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
