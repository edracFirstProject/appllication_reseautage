import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftProfilMenuComponent } from './left-profil-menu.component';

describe('LeftProfilMenuComponent', () => {
  let component: LeftProfilMenuComponent;
  let fixture: ComponentFixture<LeftProfilMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeftProfilMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftProfilMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
