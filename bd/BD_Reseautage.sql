drop database if exists Bd_Industrialisation;

create database if not exists Bd_Industrialisation;
use Bd_Industrialisation;

-- Création des tables

CREATE TABLE Utilisateur (
    id_utilisateur INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    photo_profil MEDIUMBLOB NOT NULL,
    password_util VARCHAR (255) NOT NULL,
    titre VARCHAR(255) NOT NULL,
    lieu_travail VARCHAR(255) NOT NULL,
    ville_origine VARCHAR(255) NOT NULL,
    login_Util VARCHAR(255) NOT NULL
);

CREATE TABLE Ami (
    id_ami INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    id_utilisateur INT NOT NULL,
    date_ajout DATE NOT NULL,
    FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id_utilisateur)
);

CREATE TABLE Groupe (
    id_groupe INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    createur INT NOT NULL,
    FOREIGN KEY (createur) REFERENCES Utilisateur(id_utilisateur)
);

CREATE TABLE Evenement (
    id_evenement INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    createur INT NOT NULL,
    date DATE NOT NULL,
    heure TIME NOT NULL,
    lieu VARCHAR(255) NOT NULL,
    FOREIGN KEY (createur) REFERENCES Utilisateur(id_utilisateur)
);
 

CREATE TABLE Publication (
    id_publication INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    auteur INT NOT NULL,
    contenu TEXT NOT NULL,
    date DATE NOT NULL,
    FOREIGN KEY (auteur) REFERENCES Utilisateur(id_utilisateur)
);

CREATE TABLE Commentaire (
    id_commentaire INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    auteur INT NOT NULL,
    contenu TEXT NOT NULL,
    date DATE NOT NULL,
    publication INT NOT NULL,
    FOREIGN KEY (auteur) REFERENCES Utilisateur(id_utilisateur),
    FOREIGN KEY (publication) REFERENCES Publication(id_publication)
);

CREATE TABLE Message (
    id_message INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    expediteur INT NOT NULL,
    destinataire INT NOT NULL,
    contenu TEXT NOT NULL,
    date DATE NOT NULL,
    FOREIGN KEY (expediteur) REFERENCES Utilisateur(id_utilisateur),
    FOREIGN KEY (destinataire) REFERENCES Utilisateur(id_utilisateur)
);