import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightClassListComponent } from './right-class-list.component';

describe('RightClassListComponent', () => {
  let component: RightClassListComponent;
  let fixture: ComponentFixture<RightClassListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RightClassListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RightClassListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
